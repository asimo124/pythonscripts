#!/usr/bin/python

class RomanChars():
	
	def _get_char(self, letter):
		all_chars = {
			"C": 100,
			"L": 50,
			"X": 10,
			"V": 5,
			"I": 1
		}
		return all_chars.get(letter, 1)

	def parse_roman_numeral(self, rom_number):
		i = 0
		total_num = 0     
		while 1 == 1:
			j = i + 1
			if i >= len(rom_number):
				break
			if j < len(rom_number):
				if self._get_char(rom_number[i]) < self._get_char(rom_number[j]):
					total_num = total_num + (self._get_char(rom_number[j]) - self._get_char(rom_number[i]))
					i += 2
				else:
					total_num = total_num + self._get_char(rom_number[i])
					i += 1
			else:
				total_num = total_num + self._get_char(rom_number[i])
				i += 1       
		return total_num
		
class main():
	while 1 == 1:
		rom_number = input("Enter Roman Numeral: ")
		if rom_number == "exit":
			break
		r = RomanChars()
		total_num = r.parse_roman_numeral(rom_number)
		print(total_num)
	
main()        
    
