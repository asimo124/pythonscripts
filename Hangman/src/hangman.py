#!/usr/bin/env python3 

import random

def print_guessed_phrase(current_phrase, correct_guessed_letters):
    show_phrase = ""
    for char in current_phrase:
        if char in correct_guessed_letters:
            show_phrase += char
        elif char == " " or char == "'":
            show_phrase += char
        else:
            show_phrase += "_"
    return show_phrase


guessed_letters = []
correct_guessed_letters = []
possible_phrases = ("WALTER KRONKITE", "TEENAGE MUTANT NINJA TURTLES", "REVENGE OF THE NERDS", "DONALD TRUMP", "MONSTER MASH", "OCEAN'S ELEVEN")
rand_int = random.randint(0, len(possible_phrases) - 1)
current_phrase = possible_phrases[rand_int]

allowed_incorrect_guesses = 4
incorrect_guesses = 0

while 1 == 1:
    guess2 = input("Guess a letter: ")
    if guess2 != "phrase":
        if len(guess2) > 0:
            guess = guess2[0].upper()
            if guess2.isdigit():
                print("you didn't enter a letter")
                print(print_guessed_phrase(current_phrase, correct_guessed_letters))
                continue
            
            if guess in guessed_letters:
                print("You already guessed this letter: ")
                print(print_guessed_phrase(current_phrase, correct_guessed_letters))
                continue
            
            if guess in current_phrase:
                correct_guessed_letters.append(guess)
                print("Correct!")
                print(print_guessed_phrase(current_phrase, correct_guessed_letters))
            else:
                print("Wrong. Guess again")
                print(print_guessed_phrase(current_phrase, correct_guessed_letters))
            guessed_letters.append(guess)
            continue
        else:
            print("you didn't enter a letter")
            print(print_guessed_phrase(current_phrase, correct_guessed_letters))
    else:
        phrase = input("Guess the phrase: ")
        phrase = phrase.upper()
        if current_phrase == phrase:
            print("Correct! You guessed the phrase")
            print(current_phrase)
            break
        else:
            incorrect_guesses += 1
            if allowed_incorrect_guesses == incorrect_guesses:
                print("Incorrect! You are out of guesses. You lose")
                break;
            else:
                print("Incorrect! You have ", allowed_incorrect_guesses - incorrect_guesses, " chances left.")
                print(print_guessed_phrase(current_phrase, correct_guessed_letters))
                continue
                     