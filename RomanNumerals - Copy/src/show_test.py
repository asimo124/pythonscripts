#!/usr/bin/env python3

def get_char(letter):
    all_chars = {
        "C": 100,
        "L": 50,
        "X": 10,
        "V": 5,
        "I": 1
    }
    return all_chars.get(letter, 1)

while 1 == 1:
    rom_number = input("Enter Roman Numeral: ")
    if rom_number == "exit":
        break
    
    i = 0
    total_num = 0     
    while 1 == 1:
        j = i + 1
        if i >= len(rom_number):
            break
        if j < len(rom_number):
            if get_char(rom_number[i]) < get_char(rom_number[j]):
                total_num = total_num + (get_char(rom_number[j]) - get_char(rom_number[i]))
                i += 2
            else:
                
                total_num = total_num + get_char(rom_number[i])
                i += 1
        else:
            total_num = total_num + get_char(rom_number[i])
            i += 1       
    print("rom_number: ", total_num)
    
        
    