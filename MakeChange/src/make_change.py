#!/usr/bin/env python3

import math

def checkQuarters(amount):

	quarters = int(math.floor(amount / 25))
	quarters_left = int(amount % 25)
	result = [];
	result.append(quarters);
	result.append(quarters_left)
	return result
	

def checkDimes(amount):

	dimes = int(math.floor(amount / 10))
	dimes_left = int(amount % 10)
	result = [];
	result.append(dimes);
	result.append(dimes_left)
	return result
	
	
def checkNickels(amount):

	nickels = int(math.floor(amount / 5))
	nickels_left = int(amount % 5)
	result = [];
	result.append(nickels);
	result.append(nickels_left)
	return result
	

def checkPennies(amount):

	pennies = int(math.floor(amount / 1))
	pennies_left = int(amount % 1)
	result = [];
	result.append(pennies);
	result.append(pennies_left)
	return result
	

def output_results(coin_results):

	if coin_results[0] > 0:
		print("Quarters:", coin_results[0])
	if coin_results[1] > 0:
		print("Dimes:", coin_results[1])
	if coin_results[2] > 0:
		print("Nickels:", coin_results[2])
	if coin_results[3] > 0:
		print("Pennies:", coin_results[3])
	
	
coin_results = [0, 0, 0, 0]
	
amount = input("Enter Amount: ")
amount = float(amount)
if amount > 25:
	quarter_result = checkQuarters(amount)
	quarters = quarter_result[0]
	coin_results[0] = quarters
	amount = quarter_result[1]
	
if amount > 10:
	dimes_result = checkDimes(amount)
	dimes = dimes_result[0]
	coin_results[1] = dimes
	amount = dimes_result[1]
	
if amount > 5:
	nickels_result = checkNickels(amount)
	nickels = nickels_result[0]
	coin_results[2] = nickels
	amount = nickels_result[1]
	
if amount > 0:
	pennies_result = checkPennies(amount)
	pennies = pennies_result[0]
	coin_results[3] = pennies
	amount = pennies_result[1]
	
output_results(coin_results)