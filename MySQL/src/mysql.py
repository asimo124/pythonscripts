
import _mysql
from _sqlite3 import Row

from MySQLdb.constants import FIELD_TYPE
my_conv = { FIELD_TYPE.LONG: int }
db=_mysql.connect(host="localhost",user="root", passwd="gortex!22",db="maindb", conv=my_conv)

def cleanRow(row):
    row2 = []
    for eachCol in row:
        if type(eachCol) is bytes:
            row2.append(eachCol.decode("UTF-8"))
        else:
            row2.append(eachCol)
    return row2

db.query('SELECT * from vnd_doctors')
r = db.store_result()
results = r.fetch_row()

for row in results:
    row2 = cleanRow(row)
    print(row2)
